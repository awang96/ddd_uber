package course.use_case.client;

import course.model.course.Course;
import course.model.course.EtatCourse;
import course.model.course.Frais;
import course.model.erreur.AnnulationImpossibleException;
import course.model.port.CourseRepository;
import course.use_case.fake_port.FakeCourse;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

public class AnnulerCourseShould {
    private CourseRepository courseRepository;

    @BeforeEach
    public void init(){
        courseRepository = new FakeCourse();
    }

    @Test
    void courseEnAttenteAAnnuler(){
        Course course = annulerCourse(0L);
        Assertions.assertEquals(Frais.AUCUN.getPrix(), course.getPrix());
        Assertions.assertEquals(EtatCourse.ANNULE, course.getEtat());
    }

    @Test
    void courseAccepteeAAnnuler(){
        Course course = annulerCourse(1L);
        Assertions.assertEquals(Frais.ANNULATION.getPrix(), course.getPrix());
        Assertions.assertEquals(EtatCourse.ANNULE, course.getEtat());
    }

    @Test
    void courseRecupereeAAnnuler(){
        Assertions.assertThrows(AnnulationImpossibleException.class,()->{
            annulerCourse(2L);
        });
    }

    @Test
    void courseTermineeAAnnuler(){
        Assertions.assertThrows(AnnulationImpossibleException.class,()->{
            annulerCourse(3L);
        });
    }

    private Course annulerCourse(Long courseId){
        AnnulerCourse annulerCourse = new AnnulerCourse(courseRepository);

        return annulerCourse.annuler(courseId);
    }
}
