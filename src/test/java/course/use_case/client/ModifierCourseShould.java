package course.use_case.client;

import course.model.course.Adresse;
import course.model.course.Course;
import course.model.course.TypeModificationCourse;
import course.model.erreur.ArretExistantException;
import course.model.erreur.CourseTermineException;
import course.model.erreur.MemeDestinationException;
import course.model.port.CourseRepository;
import course.use_case.fake_port.FakeCourse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Assertions;
import org.junit.jupiter.api.BeforeEach;

public class ModifierCourseShould {

    private CourseRepository courses;
    private Adresse destination;

    @BeforeEach
    public void init() {
        courses = new FakeCourse();
        destination = new Adresse(15, "rue de Belleville","Paris",75001);
    }

    @Test
    void courseImpossibleAModifier() {
        Assertions.assertThrows(CourseTermineException.class,
                () -> modifierCourse(4L, TypeModificationCourse.DESTINATION, destination));
    }

    @Test
    void destinationModifie() {
        Course courseAModifier = courses.findById(5L);
        Adresse destinationOrigin = courseAModifier.getDestination();
        Course courseModifiee = modifierCourse(5L, TypeModificationCourse.DESTINATION, destination);
        Assertions.assertNotEquals(destinationOrigin, courseModifiee.getDestination());
        Assertions.assertEquals(destination, courseModifiee.getDestination());
        Assertions.assertEquals(10, courseModifiee.getPrix());
    }

    @Test
    void ajouterArret() {
        Course courseModifie = modifierCourse(6L, TypeModificationCourse.ARRET, destination);
        Assertions.assertEquals(destination, courseModifie.getArrets().get(0));
        Assertions.assertEquals(10, courseModifie.getPrix());
    }

    @Test
    void ajouterArretExistant() {
        Course courseModifie = modifierCourse(6L, TypeModificationCourse.ARRET, destination);
        Assertions.assertEquals(destination, courseModifie.getArrets().get(0));
        Assertions.assertEquals(10, courseModifie.getPrix());
        Assertions.assertThrows(ArretExistantException.class,()->modifierCourse(6L, TypeModificationCourse.ARRET, destination));
    }

    @Test
    void memeDestination(){
        Course courseAModifier = courses.findById(5L);
        Assertions.assertThrows(MemeDestinationException.class,()->modifierCourse(5L, TypeModificationCourse.DESTINATION, courseAModifier.getDestination()));
    }

    private Course modifierCourse(Long courseId,
                                  TypeModificationCourse typeModification,
                                  Adresse destination) {
        ModifierCourse modifierCourse = new ModifierCourse(courses);

        return modifierCourse.modifier(courseId, typeModification, destination);
    }


}
