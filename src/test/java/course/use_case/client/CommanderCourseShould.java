package course.use_case.client;


import course.model.client.Client;
import course.model.client.CourseEnCours;
import course.model.course.Adresse;
import course.model.course.Course;
import course.model.course.TypeVehicule;
import course.model.erreur.CourseDejaEnCoursException;
import course.model.erreur.MemeDepartEtDestinationException;
import course.model.port.ClientRepository;
import course.model.port.CourseRepository;
import course.use_case.fake_port.FakeClient;
import course.use_case.fake_port.FakeCourse;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Assertions;

public class CommanderCourseShould{
    private ClientRepository clientRepository;
    private CourseRepository courseRepository;
    private Adresse depart;
    private Adresse destination;


    @BeforeEach
    public void init(){
        clientRepository = new FakeClient();
        courseRepository = new FakeCourse();
        depart = new Adresse(5, "rue de Paris","Paris",75001);
        destination = new Adresse(10, "rue de Belleville","Paris",75001);
    }

    @Test
    void creerCourse() {
        int prix = 5;
        Course course = commanderCourse(depart, destination, 0L, TypeVehicule.COMFORT);
        Client client = clientRepository.findById(0L);
        Assertions.assertEquals(client, course.getClient());
        Assertions.assertEquals(depart, course.getDepart());
        Assertions.assertEquals(destination, course.getDestination());
        Assertions.assertEquals(prix, course.getPrix());
        Assertions.assertEquals(CourseEnCours.OUI, course.getClient().getCourseEnCours());
    }

    @Test
    void clientADejaUneCourseEnCours() {
        Assertions.assertThrows(CourseDejaEnCoursException.class,
                () -> commanderCourse(depart, destination, 2L, TypeVehicule.COMFORT));
    }

    @Test
    void memeDepartEtDestination() {
        Assertions.assertThrows(MemeDepartEtDestinationException.class,
                () -> commanderCourse(depart, depart, 0L, TypeVehicule.COMFORT));
    }

    private Course commanderCourse(Adresse depart, Adresse destination, Long clientId, TypeVehicule vehicule) {
        CommanderCourse commanderCourse = new CommanderCourse(courseRepository, clientRepository);
        return commanderCourse.commanderCourse(depart, destination, clientId, vehicule);
    }
}
