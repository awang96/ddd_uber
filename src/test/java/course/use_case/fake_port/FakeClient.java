package course.use_case.fake_port;

import course.model.client.Client;
import course.model.client.CourseEnCours;
import course.model.port.ClientRepository;

import java.util.HashMap;
import java.util.Map;

public class FakeClient implements ClientRepository {

    Map<Long, Client> clients;

    public FakeClient(){
        clients = new HashMap<>();

        Client c = new Client(0L, "Thomas");
        c.setCourseEnCours(CourseEnCours.NON);
        clients.put(c.getId(), c);

        Client d = new Client(1L, "Alexandre");
        c.setCourseEnCours(CourseEnCours.NON);
        clients.put(d.getId(), d);

        Client clientAvecCourseEnCours = new Client();
        clientAvecCourseEnCours.setId(2L);
        clientAvecCourseEnCours.setCourseEnCours(CourseEnCours.OUI);
        clients.put(clientAvecCourseEnCours.getId(), clientAvecCourseEnCours);
    }

    @Override
    public Client findById(Long clientId){
        return clients.get(clientId);
    }

}
