package course.use_case.fake_port;

import course.model.client.Client;
import course.model.course.Adresse;
import course.model.course.Course;
import course.model.course.EtatCourse;
import course.model.port.CourseRepository;

import java.util.HashMap;
import java.util.Map;

public class FakeCourse implements CourseRepository {

    Map<Long, Course> courses;

    public FakeCourse(){
        courses = new HashMap<>();
        Adresse depart = new Adresse(5, "rue de Paris","Paris",75001);
        Adresse destination = new Adresse(10, "rue de Belleville","Paris",75001);
        Course courseAAnnuler = new Course(0L, new Client(), depart);
        courseAAnnuler.setEtat(EtatCourse.ATTENTE);
        courses.put(0L,courseAAnnuler);

        Course courseAccepteeAAnnuler = new Course(1L, new Client(), depart);
        courseAccepteeAAnnuler.setEtat(EtatCourse.ACCEPTE);
        courses.put(1L, courseAccepteeAAnnuler);

        Course courseImpossibleAAnnuler = new Course(2L, new Client(), depart);
        courseImpossibleAAnnuler.setEtat(EtatCourse.RECUPERE);
        courses.put(2L,courseImpossibleAAnnuler);

        Course courseTermineAAnnuler = new Course(3L, new Client(), depart);
        courseTermineAAnnuler.setEtat(EtatCourse.TERMINE);
        courses.put(3L, courseTermineAAnnuler);

        Course courseImpossibleAModfier = new Course(4L, new Client(), depart);
        courseImpossibleAModfier.setEtat(EtatCourse.TERMINE);
        courses.put(4L, courseImpossibleAModfier);

        Course courseDestinationAModifier = new Course(5L, new Client(), depart);
        courseDestinationAModifier.setDestination(destination);
        courses.put(5L, courseDestinationAModifier);

        Course courseAjouterArret = new Course(6L, new Client(), depart);
        courses.put(6L, courseAjouterArret);
    }

    @Override
    public Course createCourse(Adresse depart, Adresse destination, Client client){
        Course c =  new Course(1L, client, depart);
        c.setDestination(destination);
        return c;
    }

    @Override
    public Integer calculerPrix(Adresse depart, Adresse destination){
        return 5;
    }

    @Override
    public Integer reCalculerPrix(Course course) {
        return 10;
    }

    @Override
    public Course findById(Long id) {
        return courses.get(id);
    }

    @Override
    public void save(Course course) {
        System.out.println("Saved");
    }

}
