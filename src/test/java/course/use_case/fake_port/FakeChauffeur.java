package course.use_case.fake_port;

import course.model.chauffeur.Chauffeur;
import course.model.port.ChauffeurRepository;

import java.util.HashMap;
import java.util.Map;

public class FakeChauffeur implements ChauffeurRepository {

    Map<Long, Chauffeur> chauffeurs;

    public FakeChauffeur(){
        chauffeurs = new HashMap<>();
    }

    @Override
    public Chauffeur findById(Long chauffeurId){
        return chauffeurs.get(chauffeurId);
    }

}
