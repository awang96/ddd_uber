package course.use_case.client;

import course.model.client.Client;
import course.model.course.Adresse;
import course.model.course.Course;
import course.model.course.TypeVehicule;
import course.model.port.ClientRepository;
import course.model.port.CourseRepository;

public class CommanderCourse {
    private CourseRepository courses;
    private ClientRepository clients;

    public CommanderCourse(CourseRepository courses, ClientRepository clients) {
        this.courses = courses;
        this.clients = clients;
    }

    public Course commanderCourse(Adresse depart, Adresse destination, Long clientId, TypeVehicule vehicule) {
        Client client = clients.findById(clientId);
        Integer prix = courses.calculerPrix(depart, destination);
        Course course = new Course(client, depart, destination, vehicule);


        course.setPrix(prix);

        course.verifierClientAUneCourseEnCours();
        course.verifierDepartEtDestinationDifferents();

        courses.save(course);
        return course;
    }
}
