package course.use_case.client;

import course.model.course.Adresse;
import course.model.course.Course;
import course.model.port.CourseRepository;
import course.model.course.TypeModificationCourse;

public class ModifierCourse {
    private CourseRepository courses;

    public ModifierCourse(CourseRepository courses) {
        this.courses = courses;
    }

    public Course modifier(Long courseId,
                           TypeModificationCourse typeModification,
                           Adresse destination) {
        Course course = courses.findById(courseId);

        course.modifierCourse(typeModification,
                destination);
        course.setPrix(courses.reCalculerPrix(course));

        courses.save(course);
        return course;
    }
}
