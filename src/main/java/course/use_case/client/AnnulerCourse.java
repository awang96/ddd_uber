package course.use_case.client;

import course.model.course.Course;
import course.model.port.CourseRepository;

public class AnnulerCourse {
    private CourseRepository courses;

    public AnnulerCourse(CourseRepository courses){
        this.courses = courses;
    }

    public Course annuler(Long courseId){
        Course course = courses.findById(courseId);

        course.annulerCourse();

        courses.save(course);
        return course;
    }
}
