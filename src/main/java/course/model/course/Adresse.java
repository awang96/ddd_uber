package course.model.course;

public class Adresse {
    private final Integer numero;
    private final String nom;
    private final String ville;
    private final Integer codePostal;

    public Adresse(Integer numero, String nom, String ville, Integer codePostal) {
        this.numero = numero;
        this.nom = nom;
        this.ville = ville;
        this.codePostal = codePostal;
    }

    public Integer getNumero() {
        return numero;
    }

    public String getNom() {
        return nom;
    }

    public String getVille() {
        return ville;
    }

    public Integer getCodePostal() {
        return codePostal;
    }
}
