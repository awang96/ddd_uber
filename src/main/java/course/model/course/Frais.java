package course.model.course;

import java.util.HashMap;
import java.util.Map;

public enum Frais {
    AUCUN(0),ANNULATION(5);

    private Integer prix;

    Frais(Integer prix) {
        this.prix = prix;
    }
    public Integer getPrix() {
        return prix;
    }
}
