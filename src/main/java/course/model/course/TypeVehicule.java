package course.model.course;

public enum TypeVehicule {

    UBERX,
    GREEN,
    COMFORT,
    VAN

}
