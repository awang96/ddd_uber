package course.model.course;

import course.model.chauffeur.Chauffeur;
import course.model.client.Client;
import course.model.client.CourseEnCours;
import course.model.erreur.*;

import java.util.ArrayList;

public class Course {

    private long id;
    private final Client client;
    private Chauffeur chauffeur;
    private Adresse destination;
    private final Adresse depart;
    private Integer prix;
    private EtatCourse etat;
    private final ArrayList<Adresse> arrets;
    private TypeVehicule vehicule;

    public Course(Long id, Client client, Adresse depart) {
        this.etat = EtatCourse.ATTENTE;
        this.arrets = new ArrayList<Adresse>();
        this.id = id;
        this.client = client;
        this.depart = depart;
    }

    public Course(Client client, Adresse depart, Adresse destination, TypeVehicule typeVehicule) {
        this.etat = EtatCourse.ATTENTE;
        this.arrets = new ArrayList<Adresse>();
        this.client = client;
        this.depart = depart;
        this.destination = destination;
        this.vehicule = typeVehicule;
    }

    public EtatCourse getEtat() {
        return etat;
    }

    public void setEtat(EtatCourse etat) {
        this.etat = etat;
    }

    public Client getClient() {
        return client;
    }

    public Adresse getDestination() {
        return destination;
    }

    public void setDestination(Adresse destination) {
        this.destination = destination;
    }

    public Adresse getDepart() {
        return depart;
    }

    public Integer getPrix() {
        return prix;
    }

    public void setPrix(Integer prix) {
        this.prix = prix;
    }

    public ArrayList<Adresse> getArrets() {
        return arrets;
    }

    public void ajouterArret(Adresse arret) {
        arrets.add(arret);
    }

    public void annulerCourse() {
        switch(this.getEtat()){
            case ATTENTE:
                this.setEtat(EtatCourse.ANNULE);
                this.setPrix(Frais.AUCUN.getPrix());
                break;
            case ACCEPTE:
                this.setEtat(EtatCourse.ANNULE);
                this.setPrix(Frais.ANNULATION.getPrix());
                break;
            default:
                throw new AnnulationImpossibleException();
        }
    }

    public void modifierCourse(TypeModificationCourse typeModification, Adresse destination) {
        if (this.getEtat() == EtatCourse.TERMINE) {
            throw new CourseTermineException();
        }
        if (typeModification == TypeModificationCourse.ARRET) {
            for(Adresse adresse:this.getArrets()){
                if(adresse==destination){
                    throw  new ArretExistantException();
                }
            }
            this.ajouterArret(destination);
        } else {
            if(this.getDestination() == destination){
                throw new MemeDestinationException();
            }
            this.setDestination(destination);
        }
    }

    public void verifierClientAUneCourseEnCours() {
        if (this.getClient().getCourseEnCours() == CourseEnCours.OUI) {
            throw new CourseDejaEnCoursException();
        } else {
            this.getClient().setCourseEnCours(CourseEnCours.OUI);
        }
    }

    public void verifierDepartEtDestinationDifferents() {
        if (this.getDepart() == this.getDestination()) {
            throw new MemeDepartEtDestinationException();
        }
    }
}
