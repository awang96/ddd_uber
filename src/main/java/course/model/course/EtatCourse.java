package course.model.course;

public enum EtatCourse {
    ATTENTE,
    ACCEPTE,
    ANNULE,
    RECUPERE,
    TERMINE
}
