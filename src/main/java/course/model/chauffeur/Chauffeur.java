package course.model.chauffeur;

public class Chauffeur {

    private Long id;
    private String nom;
    private String voiture;
    private Integer note;

    public Chauffeur() {
    }

    public Chauffeur(Long id,String nom, String voiture, Integer note) {
        this.id = id;
        this.nom = nom;
        this.voiture = voiture;
        this.note = note;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getVoiture() {
        return voiture;
    }

    public void setVoiture(String voiture) {
        this.voiture = voiture;
    }

    public Integer getNote() {
        return note;
    }

    public void setNote(Integer note) {
        this.note = note;
    }
}
