package course.model.client;

public class Client {

    private Long id;
    private String nom;
    private CourseEnCours courseEnCours;

    public Client() {
    }

    public Client(Long id, String nom) {
        this.id = id;
        this.nom = nom;
    }


    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public CourseEnCours getCourseEnCours() {
        return courseEnCours;
    }

    public void setCourseEnCours(CourseEnCours courseEnCours) {
        this.courseEnCours = courseEnCours;
    }
}
