package course.model.port;

import course.model.course.Adresse;
import course.model.client.Client;
import course.model.course.Course;

public interface CourseRepository {

    Course createCourse(Adresse depart, Adresse destination, Client client);

    Integer calculerPrix(Adresse depart, Adresse destination);

    Integer reCalculerPrix(Course course);

    Course findById(Long id);

    void save(Course course);

}
