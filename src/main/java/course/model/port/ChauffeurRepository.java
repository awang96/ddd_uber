package course.model.port;

import course.model.chauffeur.Chauffeur;

public interface ChauffeurRepository {
    Chauffeur findById(Long id);
}
