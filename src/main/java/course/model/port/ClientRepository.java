package course.model.port;

import course.model.client.Client;

public interface ClientRepository {

    Client findById(Long id);
}
